from flask import Flask, render_template, request, abort

app = Flask(__name__)

@app.route('/<name>') # from lecture slides
def hello(name):
    try: # credit to Kevin Post from Piazza for giving me the idea to use try/except logic
        return render_template(name, nameHtml=name) # taken and swapped out appropriate variable names from lecture slides
    except:
        abort(404) # appropriate abort libary and function, along with parameter, taken from flask-restplus website

@app.errorhandler(404) # most of this function is inspired from lecture slide and classmates on Piazza
def error403(e):
    url = request.full_path
    if ((url.find("//") != -1) or (url.find("..") != -1) or (url.find("~") != -1)):
        return render_template('403.html')
    return render_template('404.html')

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')

