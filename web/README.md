Program is intended to utilize "file checking" logic like in project 1, however, this time
utilizing the Flask framework.
Basic logic goes as following:
if file is found, we pass the file through with the render template function
if file is not found, we pass a 404.html file located in appropriate directory, to the client
inappropriate characters were handled with transmission of a 403.html file

## Author: Adrian Sierra, asierra4@uoregon.edu ##
